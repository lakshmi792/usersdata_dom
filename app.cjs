
fetch('https://fakestoreapi.com/users')
    .then((value) => {
        return value.json();
    })
    .then((data) => {
        if (data) {
            console.log("successfully fetched");
            users(data);
            loader_roller.classList.remove('lds-roller');
            loaderIcon.classList.remove('loader');
        } else {
            loader_roller.classList.remove('lds-roller');
            loaderIcon.classList.remove('loader');
            noData();
        }
    })
    .catch((err) => {
        console.error("Error,Something went wrong");
        console.error(err);
        loader_roller.classList.remove('lds-roller');
        loaderIcon.classList.remove('loader');
        errorData();
    });

let loaderIcon = document.querySelector('.loader');
let loader_roller = document.querySelector('.lds-roller');


function users(data) {
    let ul = document.querySelector('ul');

    if (Array.isArray(data)) {
        data.forEach((values) => {
            let li = document.createElement('li');
            let id = document.createElement('p');
            id.classList.add("usersCard");

            id.textContent = `Id: ${values.id}`;
            let fullName = document.createElement('p');
            fullName.classList.add("usersCard");
            fullName.textContent = "Fullname: " + `${values.name.firstname} ${values.name.lastname}`.toUpperCase();

            let userName = document.createElement('p');
            userName.classList.add("usersCard");
            userName.textContent = `Username: ${values.username}`;

            let phone = document.createElement('p');
            phone.classList.add("usersCard");
            phone.textContent = `Phone: ${values.phone}`;

            let address = document.createElement('p');
            address.classList.add("usersCard");
            address.textContent = `Address:  ${values.address.number} ${values.address.city} ${values.address.street} ${values.address.zipcode}`;

            li.append(id, fullName, userName, phone, address);
            ul.append(li);
        });
    } else {
        let infoArray = [];
        infoArray.push(data);

        users(infoArray)
    }
}


function noData() {
    let ul = document.querySelector('ul');
    let divData = document.createElement('div');
    let h2 = document.createElement('h2');
    divData.classList.add("no-data");
    h2.textContent = 'No data is present';
    divData.append(h2);
    ul.append(divData);
}


function errorData() {
    let ul = document.querySelector('ul');
    let error = document.createElement('div');
    let h2 = document.createElement('h2');
    error.classList.add("error");
    h2.textContent = 'Failed to fetch the data';
    error.append(h2);
    ul.append(error);
}

